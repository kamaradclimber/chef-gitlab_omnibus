## Things this cookbook *doesn't* do:

* Manage a firewall
* Install SSH or Postfix
* Manage secrets (database passwords, SSL keys/certs, etc)

### Why not?

This is a library/application cookbook. It's sole purpose is installation
and configuration of GitLab and/or GitLab CI. The goal is to be forward
compatible with future versions of GitLab and avoid assumptions about how
users like to use Chef. Therefore, it does not validate whether configuration
hash values are valid for GitLab Omnibus, it does not require any data bags,
manage secrets, install SSL certificates, or anything else of that nature.
This leaves users free to wrap the cookbook and add those bits that work for
their environment.
